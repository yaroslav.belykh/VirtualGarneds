#include "application.h"

Application::Application(QObject *parent) : QObject(parent)
{

}

void Application::MoveLights(RGB_t *LEDs, uint8_t countLEDs, RGB_t *Stage, uint8_t countStage, uint16_t *diffIndex, uint8_t reverse)
{
    int rgbInt=0;
    //taskENTER_CRITICAL();
    for (uint8_t i=0; i < int(countLEDs); i++)
    {
        if (reverse==1)
            {rgbInt=(i+*diffIndex)%countStage;}
        else
        {
            rgbInt=(i-*diffIndex)%countStage;
            if (rgbInt<0)
                {rgbInt += countStage;}
        }
        LEDs[i]=Stage[rgbInt];
    }
    //taskEXIT_CRITICAL();
    if (*diffIndex+1<countStage)
        {*diffIndex += 1;}
    else
        {*diffIndex=0;}
}

void Application::MoveColor(HSV_t *LEDs, uint8_t countLEDs, HSV_t *Stage, uint16_t countStage, uint16_t *diffIndex, uint8_t reverse)
{
    if (*diffIndex==0)
    {
        uint8_t j=0;
        //taskENTER_CRITICAL();
        for (uint16_t i=0; i< countLEDs; i++)
        {
            LEDs[i]=Stage[j];
            if (j+1<countStage)
                {j += 1;}
            else
                {j = 0;}
        }
        //taskEXIT_CRITICAL();
        *diffIndex = 1;
    }
    else
    {
        //taskENTER_CRITICAL();
        for (uint8_t i=0; i< countLEDs; i++)
        {
            if (reverse==0)
            {
                if (LEDs[i].h<359)
                    {LEDs[i].h += 1;}
                else
                    {LEDs[i].h = 0;}
            }
            else
            {
                if (LEDs[i].h>0)
                    {LEDs[i].h -= 1;}
                else
                    {LEDs[i].h = 359;}
            }
        }
        //taskEXIT_CRITICAL();
    }
    if (*diffIndex+1<65535  )
        {*diffIndex += 1;}
    else
        {*diffIndex=0;}
}

void Application::RainbowWave(HSV_t *LEDs, uint8_t countLEDs, HSV_t *Stage, uint8_t countStage, uint16_t *diffIndex, uint16_t chIndex, uint16_t maxWave, uint8_t *firstRun)
{
    uint8_t maxLeds;
    if (countLEDs>=countStage)
        {maxLeds=countStage;}
    else
        {maxLeds=countLEDs;}
    if (*firstRun==0)
    {
        //taskENTER_CRITICAL();
        for (uint8_t i=0; i< maxLeds; i++)
            {LEDs[i]=Stage[i];}
        for (int i=maxLeds;i<countLEDs;i++)
        {
            LEDs[i].h=0;
            LEDs[i].s=0;
            LEDs[i].v=0;
        }
        //taskEXIT_CRITICAL();
        *firstRun = maxLeds;
    }
    else
    {
        //taskENTER_CRITICAL();
        for (uint8_t i=0; i<countLEDs; i++)
        {
            if ((maxWave>0) && i==*diffIndex+1-maxWave)
            {
                LEDs[i].h=0;
                LEDs[i].s=0;
                LEDs[i].v=0;
            }
            else
            {
                if (LEDs[i].s!=0 && LEDs[i].v!=0)
                {
                    if (LEDs[i].h<359)
                        {LEDs[i].h += 1;}
                    else
                        {LEDs[i].h = 0;}
                }
            }
        }
        //taskEXIT_CRITICAL();
        if (*firstRun<countLEDs)
        {
            if (LEDs[*firstRun-1].h>=chIndex)
            {
                uint8_t j=0;
                uint8_t imaxLeds;
                if (*firstRun+maxLeds<countLEDs)
                    {imaxLeds=*firstRun+maxLeds;}
                else
                    {imaxLeds=countLEDs;}
                //taskENTER_CRITICAL();
                for (int i=*firstRun; i<imaxLeds; i++)
                {
                    if (j<countStage)
                    {
                        LEDs[i]=Stage[j];
                        j += 1;
                    }
                }
                //taskEXIT_CRITICAL();
                *firstRun = imaxLeds;
                *diffIndex += 1;
            }
        }
        else
        {
            if (maxWave>0)
            {
                if (LEDs[*firstRun-1].h%chIndex==0)
                    {*diffIndex += 1;}
            }
        }
    }
    if (*diffIndex+1-maxWave>countLEDs+2)
    {
        *firstRun=0;
        *diffIndex=0;
    }
    if (uint32_t(*diffIndex+1)==65536  )
    {*diffIndex=0;}
}

void Application::MoveHSV(HSV_t *LEDs, uint8_t countLEDs, HSV_t *Stage, uint8_t countStage, uint16_t *diffIndex)
{
    switch (*diffIndex%3)
    {
        case 0:
        {
            if (*diffIndex/3>=countStage)
                {*diffIndex=0;}
            //taskENTER_CRITICAL();
            for (uint8_t i=0; i< countLEDs; i++)
                {LEDs[i]=Stage[*diffIndex/3];}
            //taskEXIT_CRITICAL();
            *diffIndex += 1;
        }
        break;
        case 1:
        {
            if (LEDs[0].v!=255)
            {
                //taskENTER_CRITICAL();
                for (uint8_t i=0; i< countLEDs; i++)
                    {LEDs[i].v += 1;}
                //taskEXIT_CRITICAL();
            }
            else
                {*diffIndex += 1;}
        }
        break;
        case 2:
        {
            if (LEDs[0].v!=0)
            {
                //taskENTER_CRITICAL();
                for (uint8_t i=0; i< countLEDs; i++)
                    {LEDs[i].v -= 1;}
                //taskEXIT_CRITICAL();
            }
            else
                {*diffIndex += 1;}
        }
        break;
        default:
        break;
    }
}

void Application::UpDownMove(HSV_t *LEDs, uint8_t countLEDs, HSV_t *Stage, uint16_t countStage, uint8_t squage, uint16_t *diffIndex, uint8_t reverse)
{
    if (*diffIndex==0)
    {
        uint8_t j=0;
        //taskENTER_CRITICAL();
        for (uint16_t i=0; i< countLEDs; i++)
        {
            LEDs[i]=Stage[j];
            if (j+1<countStage)
                {j += 1;}
            else
                {j = 0;}
        }
        //taskEXIT_CRITICAL();
        *diffIndex = 1;
    }
    else
    {
        for (int i=0;i<int(countLEDs);i++)
        {
            //if (i==countLEDs%countStage)
        }
    }
}

void Application::setOneColorLeds_rgb(RGB_t *LEDs, uint8_t countLEDs, uint8_t rgbRed, uint8_t rgbGreen, uint8_t rgbBlue)
{
    //taskENTER_CRITICAL();
    for (int i=0;i<int(countLEDs);i++)
    {
        LEDs[i].r=rgbRed;
        LEDs[i].g=rgbGreen;
        LEDs[i].b=rgbBlue;
    }
    //taskEXIT_CRITICAL();
}

void Application::setOneColorLeds_hsv(HSV_t *LEDs, uint8_t countLEDs, uint16_t hsvHue, uint8_t hsvSaturation, uint8_t hsvValue)
{
    for (int i=0;i<int(countLEDs);i++)
    {
        LEDs[i].h=hsvHue;
        LEDs[i].s=hsvSaturation;
        LEDs[i].v=hsvValue;
    }
}

