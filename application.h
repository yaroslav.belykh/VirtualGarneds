#ifndef APPLICATION_H
#define APPLICATION_H

#include <QObject>
#include <QDebug>
#include "structure_led.h"

class Application : public QObject
{
    Q_OBJECT
public:
    explicit Application(QObject *parent = nullptr);

    void MoveLights(RGB_t *LEDs, uint8_t countLEDs, RGB_t *Stage, uint8_t countStage, uint16_t *diffIndex, uint8_t reverse);
    void MoveColor(HSV_t *LEDs, uint8_t countLEDs, HSV_t *Stage, uint16_t countStage, uint16_t *diffIndex, uint8_t reverse);
    void RainbowWave(HSV_t *LEDs, uint8_t countLEDs, HSV_t *Stage, uint8_t countStage, uint16_t *diffIndex, uint16_t chIndex, uint16_t maxWave, uint8_t *firstRun);
    void MoveHSV(HSV_t *LEDs, uint8_t countLEDs, HSV_t *Stage, uint8_t countStage, uint16_t *diffIndex);
    void UpDownMove(HSV_t *LEDs, uint8_t countLEDs, HSV_t *Stage, uint16_t countStage, uint8_t squage, uint16_t *diffIndex, uint8_t reverse);

    void setOneColorLeds_rgb(RGB_t *LEDs, uint8_t countLEDs, uint8_t rgbRed, uint8_t rgbGreen, uint8_t rgbBlue);
    void setOneColorLeds_hsv(HSV_t *LEDs, uint8_t countLEDs, uint16_t hsvHue, uint8_t hsvSaturation, uint8_t hsvValue);

signals:

public slots:
};

#endif // APPLICATION_H
