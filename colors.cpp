#include "colors.h"

Colors::Colors(QObject *parent) : QObject(parent)
{

}

RGB_t Colors::setRed_rgb()
{
    RGB_t vColor;
    vColor.r=255;
    vColor.g=0;
    vColor.b=0;
    return vColor;
}

RGB_t Colors::setGreen_rgb()
{
    RGB_t vColor;
    vColor.r=0;
    vColor.g=255;
    vColor.b=0;
    return vColor;
}

RGB_t Colors::setBlue_rgb()
{
    RGB_t vColor;
    vColor.r=0;
    vColor.g=0;
    vColor.b=255;
    return vColor;
}

HSV_t Colors::setRed_hsv()
{
    HSV_t vColor;
    vColor.h=0;
    vColor.s=255;
    vColor.v=255;
    return vColor;
}

HSV_t Colors::setGreen_hsv()
{
    HSV_t vColor;
    vColor.h=120;
    vColor.s=255;
    vColor.v=255;
    return vColor;
}

HSV_t Colors::setBlue_hsv()
{
    HSV_t vColor;
    vColor.h=240;
    vColor.s=255;
    vColor.v=255;
    return vColor;
}

CMYK_t Colors::setRed_cmyk()
{
    CMYK_t vColor;
    vColor.c=0;
    vColor.m=100;
    vColor.y=100;
    vColor.k=0;
    return vColor;
}

CMYK_t Colors::setGreen_cmyk()
{
    CMYK_t vColor;
    vColor.c=100;
    vColor.m=0;
    vColor.y=100;
    vColor.k=0;
    return vColor;
}

CMYK_t Colors::setBlue_cmyk()
{
    CMYK_t vColor;
    vColor.c=100;
    vColor.m=100;
    vColor.y=0;
    vColor.k=0;
    return vColor;
}

RGB_t Colors::setCyan_rgb()
{
    RGB_t vColor;
    vColor.r=0;
    vColor.g=255;
    vColor.b=255;
    return vColor;
}

RGB_t Colors::setMagenta_rgb()
{
    RGB_t vColor;
    vColor.r=255;
    vColor.g=0;
    vColor.b=255;
    return vColor;
}

RGB_t Colors::setYellow_rgb()
{
    RGB_t vColor;
    vColor.r=255;
    vColor.g=255;
    vColor.b=0;
    return vColor;
}

RGB_t Colors::setBlack_rgb()
{
    RGB_t vColor;
    vColor.r=0;
    vColor.g=0;
    vColor.b=0;
    return vColor;
}

HSV_t Colors::setCyan_hsv()
{
    HSV_t vColor;
    vColor.h=180;
    vColor.s=255;
    vColor.v=255;
    return vColor;
}

HSV_t Colors::setMagenta_hsv()
{
    HSV_t vColor;
    vColor.h=300;
    vColor.s=255;
    vColor.v=255;
    return vColor;
}

HSV_t Colors::setYellow_hsv()
{
    HSV_t vColor;
    vColor.h=60;
    vColor.s=255;
    vColor.v=255;
    return vColor;
}

HSV_t Colors::setBlack_hsv()
{
    HSV_t vColor;
    vColor.h=0;
    vColor.s=0;
    vColor.v=0;
    return vColor;
}

CMYK_t Colors::setCyan_cmyk()
{
    CMYK_t vColor;
    vColor.c=100;
    vColor.m=0;
    vColor.y=0;
    vColor.k=0;
    return vColor;
}

CMYK_t Colors::setMagenta_cmyk()
{
    CMYK_t vColor;
    vColor.c=0;
    vColor.m=100;
    vColor.y=0;
    vColor.k=0;
    return vColor;
}

CMYK_t Colors::setYellow_cmyk()
{
    CMYK_t vColor;
    vColor.c=0;
    vColor.m=0;
    vColor.y=100;
    vColor.k=0;
    return vColor;
}

CMYK_t Colors::setBlack_cmyk()
{
    CMYK_t vColor;
    vColor.c=0;
    vColor.m=0;
    vColor.y=0;
    vColor.k=100;
    return vColor;
}

RGB_t Colors::setWhight_rgb()
{
    RGB_t vColor;
    vColor.r=255;
    vColor.g=255;
    vColor.b=255;
    return vColor;
}

HSV_t Colors::setWhight_hsv()
{
    HSV_t vColor;
    vColor.h=0;
    vColor.s=255;
    vColor.v=0;
    return vColor;
}

CMYK_t Colors::setWhite_cmyk()
{
    CMYK_t vColor;
    vColor.c=0;
    vColor.m=0;
    vColor.y=0;
    vColor.k=0;
    return vColor;
}

RGB_t Colors::setViolet_rgb()
{
    RGB_t vColor;
    vColor.r=143;
    vColor.g=0;
    vColor.b=255;
    return vColor;
}

HSV_t Colors::setViolet_hsv()
{
    HSV_t vColor;
    vColor.h=270;
    vColor.s=255;
    vColor.v=255;
    return vColor;
}

RGB_t Colors::setOrange_rgb()
{
    RGB_t vColor;
    vColor.r=255;
    vColor.g=128;
    vColor.b=0;
    return vColor;
}

HSV_t Colors::setOrange_hsv()
{
    HSV_t vColor;
    vColor.h=30;
    vColor.s=255;
    vColor.v=255;
    return vColor;
}

RGB_t Colors::setRose_rgb()
{
    RGB_t vColor;
    vColor.r=255;
    vColor.g=0;
    vColor.b=127;
    return vColor;
}

RGB_t Colors::setEmerald_rgb()
{
    RGB_t vColor;
    vColor.r=80;
    vColor.g=200;
    vColor.b=120;
    return vColor;
}

RGB_t Colors::setAmber_rgb()
{
    RGB_t vColor;
    vColor.r=255;
    vColor.g=191;
    vColor.b=0;
    return vColor;
}

RGB_t Colors::setUltramarine_rgb()
{
    RGB_t vColor;
    vColor.r=18;
    vColor.g=10;
    vColor.b=143;
    return vColor;
}
