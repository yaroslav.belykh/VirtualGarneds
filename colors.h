#ifndef COLORS_H
#define COLORS_H

#include <QObject>

#include "structure_led.h"

class Colors : public QObject
{
    Q_OBJECT
public:
    explicit Colors(QObject *parent = nullptr);

    // RGB
    RGB_t setRed_rgb(void);
    RGB_t setGreen_rgb(void);
    RGB_t setBlue_rgb(void);

    HSV_t setRed_hsv(void);
    HSV_t setGreen_hsv(void);
    HSV_t setBlue_hsv(void);

    CMYK_t setRed_cmyk(void);
    CMYK_t setGreen_cmyk(void);
    CMYK_t setBlue_cmyk(void);

    // CMYK
    RGB_t setCyan_rgb(void);
    RGB_t setMagenta_rgb(void);
    RGB_t setYellow_rgb(void);
    RGB_t setBlack_rgb(void);

    HSV_t setCyan_hsv(void);
    HSV_t setMagenta_hsv(void);
    HSV_t setYellow_hsv(void);
    HSV_t setBlack_hsv(void);

    CMYK_t setCyan_cmyk(void);
    CMYK_t setMagenta_cmyk(void);
    CMYK_t setYellow_cmyk(void);
    CMYK_t setBlack_cmyk(void);

    // Basic
    RGB_t setWhight_rgb(void);
    HSV_t setWhight_hsv(void);
    CMYK_t setWhite_cmyk(void);

    RGB_t setViolet_rgb(void);
    HSV_t setViolet_hsv(void);
    RGB_t setOrange_rgb(void);
    HSV_t setOrange_hsv(void);

    // Deep Color
    RGB_t setRose_rgb(void);
    //HSV_t setRose_hsv(void);
    RGB_t setEmerald_rgb(void);
    //HSV_t setEmerald_hsv(void);
    RGB_t setAmber_rgb(void);
    //HSV_t setAmber_hsv(void);
    RGB_t setUltramarine_rgb(void);
    //RGB_t setVermilion_rgb(void);
    //RGB_t setPurple_rgb(void);

    // Light Color
    /*RGB_t setPink_rgb(void);
    RGB_t setLightMint_rgb(void);
    RGB_t setLightYellow_rgb(void);
    RGB_t setSkyBlue_rgb(void);
    HSV_t setSkyBlue_hsv(void);

    // Under Color
    RGB_t setRaspberry_rgb(void);
    RGB_t setAquamarine_rgb(void);*/


signals:

public slots:
};

#endif // COLORS_H
