#include "ledsgarlend.h"

LedsGarlend::LedsGarlend(tLEDs *Ls, QWidget *widget, QObject *parent) : QObject(parent)
{
    this->setParent(parent);
    LEDs=Ls;
    initLeds(widget);
}

LedsGarlend::~LedsGarlend()
{
    /*for (int i=0;i<int(NUM_LEDS);i++)
    {
        LEDs->LED[i]->deleteLater();
        LEDs->LED[i]=0;
    }*/
    LEDs->LED.clear();
}

void LedsGarlend::setRGBState(RGB_t rgb[NUM_LEDS])
{
    for (int i=0;i<int(NUM_LEDS);i++)
    {
        QColor Color;
        Color.setRgb(int(rgb[i].r),int(rgb[i].g),int(rgb[i].b),255);
        LEDs->LED[i]->setColor(Color);
    }
}

void LedsGarlend::setHSVState(HSV_t hsv[NUM_LEDS])
{
    for (int i=0;i<int(NUM_LEDS);i++)
    {
        QColor Color;
        Color.setHsv(int(hsv[i].h),int(hsv[i].s),int(hsv[i].v),255);
        LEDs->LED[i]->setColor(Color);
    }
}

void LedsGarlend::initLeds(QWidget* wgx)
{
    for (int i=0;i<int(NUM_LEDS);i++)
    {
        LEDs->LED[i]=new VirtualLed(wgx);
        LEDs->LED[i]->setObjectName(QStringLiteral("LED_")+QVariant(i).toString());
        LEDs->LED[i]->setSize(16,16);
    }
}
