#ifndef LEDSGARLEND_H
#define LEDSGARLEND_H

#include <QObject>
#include <QVariant>

#include "structure_led.h"
#include "structure_widgets.h"

class LedsGarlend : public QObject
{
    Q_OBJECT
public:
    explicit LedsGarlend(tLEDs *LEDs, QWidget*, QObject *parent = nullptr);
    ~LedsGarlend();

    void setRGBState(RGB_t[NUM_LEDS]);
    void setHSVState(HSV_t[NUM_LEDS]);

signals:

public slots:

private:
    tLEDs *LEDs;
    inline void initLeds(QWidget*);
};

#endif // LEDSGARLEND_H
