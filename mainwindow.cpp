#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    LG=new LedsGarlend(&LEDs,this,this);
    restoreGarlend(this->size(),false);
    connect(this,SIGNAL(sizeChanged(QSize,bool)),this,SLOT(restoreGarlend(QSize,bool)));

    Timer = new QTimer;
    app = new Application();
    ac = new MakeColorsArray();
    ui->cmbBox_Stage->addItem("Auto");

    //connect(Timer, SIGNAL(timeout()),this,SLOT(timeoutDebug()));
    ui->spinBox->setMaximum(int(NUM_LEDS));
    Timer->setSingleShot(false);
}

MainWindow::~MainWindow()
{
    app->deleteLater();
    ac->deleteLater();
    disconnect(this,SIGNAL(sizeChanged(QSize,bool)),this,SLOT(restoreGarlend(QSize,bool)));
    //disconnect(Timer, SIGNAL(timeout()),this,SLOT(timeoutDebug()));
    LG->deleteLater();
    LG=0;
    Timer->deleteLater();
    delete ui;
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    QMainWindow::resizeEvent(event);
    emit sizeChanged(event->size(), true);
}

void MainWindow::doActions()
{
    ui->cmbBox_Stage->setEnabled(false);
    ui->cmbBox_Color->setDisabled(true);
    DeferenceIndex=0;
    fTimer=0;

    if (currStadia!=0)
    {
        app->setOneColorLeds_rgb(leds_rgb, NUM_LEDS, (uint8_t) 0, (uint8_t) 0, (uint8_t) 0);
        LG->setRGBState(leds_rgb);
        if (isRgb)
        {
            switch (currStadia) {
                case 1:
                {
                    connect(Timer, SIGNAL(timeout()), this, SLOT(stadia_rgb_1()));
                    ui->spinBox->setEnabled(false);
                }
                break;
                case 2:
                {
                    connect(Timer, SIGNAL(timeout()), this, SLOT(stadia_rgb_2()));
                }
                break;
                case 3:
                {
                    connect(Timer, SIGNAL(timeout()), this, SLOT(stadia_rgb_3()));
                }
                break;
                default:
                break;
            }
        }
        if (isHsv)
        {
            switch (currStadia) {
                case 1:
                {
                    connect(Timer, SIGNAL(timeout()), this, SLOT(stadia_hsv_1()));
                    ui->spinBox->setEnabled(false);
                }
                break;
                case 2:
                {
                    connect(Timer, SIGNAL(timeout()), this, SLOT(stadia_hsv_2()));
                }
                break;
                case 3:
                {
                    connect(Timer, SIGNAL(timeout()), this, SLOT(stadia_hsv_3()));
                    ui->spinBox->setEnabled(false);
                }
                break;
                case 4:
                {
                    connect(Timer, SIGNAL(timeout()), this, SLOT(stadia_hsv_4()));
                    ui->spinBox->setEnabled(false);
                }
                break;
                case 5:
                {
                    connect(Timer, SIGNAL(timeout()), this, SLOT(stadia_hsv_5()));
                    ui->spinBox->setEnabled(false);
                }
                break;
                default:
                break;
            }
        }
        vTaskDelay(TQR/2);
    }
}

void MainWindow::doneActions()
{
    Timer->stop();
    if (currStadia!=0)
    {
        if (isRgb)
        {
            switch (currStadia) {
                case 1:
                {
                    ui->spinBox->setEnabled(true);
                    disconnect(Timer, SIGNAL(timeout()), this, SLOT(stadia_rgb_1()));
                }
                break;
                case 2:
                {
                    disconnect(Timer, SIGNAL(timeout()), this, SLOT(stadia_rgb_2()));
                }
                break;
                case 3:
                {
                    disconnect(Timer, SIGNAL(timeout()), this, SLOT(stadia_rgb_3()));
                }
                break;
                default:
                break;
            }
        }
        if (isHsv)
        {
            switch (currStadia) {
                case 1:
                {
                    disconnect(Timer, SIGNAL(timeout()), this, SLOT(stadia_hsv_1()));
                    ui->spinBox->setEnabled(true);
                }
                break;
                case 2:
                {
                    disconnect(Timer, SIGNAL(timeout()), this, SLOT(stadia_hsv_2()));
                }
                break;
                case 3:
                {
                    disconnect(Timer, SIGNAL(timeout()), this, SLOT(stadia_hsv_3()));
                    ui->spinBox->setEnabled(true);
                }
                break;
                case 4:
                {
                    disconnect(Timer, SIGNAL(timeout()), this, SLOT(stadia_hsv_4()));
                    ui->spinBox->setEnabled(true);
                }
                break;
                case 5:
                {
                    disconnect(Timer, SIGNAL(timeout()), this, SLOT(stadia_hsv_4()));
                    ui->spinBox->setEnabled(true);
                }
                break;
                default:
                break;
            }
        }
    }
    ui->cmbBox_Stage->setEnabled(true);
    ui->cmbBox_Color->setDisabled(false);
}

void MainWindow::timeoutDebug()
{
    int tmInterval;
    tmInterval=Timer->interval();
    int i;
    i=0;
    i++;
}

void MainWindow::restoreGarlend(QSize WidgetSize, bool needClear)
{
    int lineCount;
    int currentLine;
    QSize LedSize;
    if (needClear)
    {
        for (int i=0;i<LEDs.LED.keys().count();i++)
            {ui->CentralLayout->removeWidget(LEDs.LED[LEDs.LED.keys().at(i)]);}
    }
    LedSize=LEDs.LED.first()->size();
    lineCount=(WidgetSize.width()-8)/(LedSize.width()+4);
    currentLine=0;
    for (int i=0;i<LEDs.LED.keys().count();i++)
    {
        if (i-currentLine*lineCount==lineCount)
            {currentLine++;}
        if (currentLine%2==0)
            {ui->CentralLayout->addWidget(LEDs.LED[LEDs.LED.keys().at(i)],currentLine,i-currentLine*lineCount);}
        else
            {ui->CentralLayout->addWidget(LEDs.LED[LEDs.LED.keys().at(i)],currentLine,lineCount-(i-currentLine*lineCount)-1);}
    }
}



void MainWindow::on_pushButton_toggled(bool checked)
{
    if (ui->cmbBox_Color->currentIndex()==0)
    {
        int ind;
        ind=getValue(1,2);
        ui->cmbBox_Color->setCurrentIndex(ind);
        on_cmbBox_Color_currentIndexChanged(ind);
    }
    if (ui->cmbBox_Stage->currentIndex()==0)
    {
        int ind;
        if (ui->cmbBox_Color->currentIndex()==1)
            {ind=getValue(1,2);}
        else
            {ind=getValue(1,3);}
        ui->cmbBox_Stage->setCurrentIndex(ind);
        on_cmbBox_Stage_currentIndexChanged(ind);
    }
    if (checked)
    {
        ui->pushButton->setText("Stop");
        doActions();
    }
    else
    {
        ui->pushButton->setText("Start");
        doneActions();
    }
}

void MainWindow::on_cmbBox_Color_currentIndexChanged(int index)
{
    ui->cmbBox_Stage->clear();
    ui->cmbBox_Stage->addItem("Auto");
    switch (index)
    {
        case 0:
        {
            isRgb=false;
            isHsv=false;
            isCmyk=false;
        }
        break;
        case 1:
        {
            isRgb=true;
            isHsv=false;
            isCmyk=false;
            ui->cmbBox_Stage->addItem("Lights Move");
            ui->cmbBox_Stage->addItem("Lights Move RGB");
            ui->cmbBox_Stage->addItem("Lights Move CMYK");
        }
        break;
        case 2:
        {
            isRgb=false;
            isHsv=true;
            isCmyk=false;
            ui->cmbBox_Stage->addItem("Color Move");
            ui->cmbBox_Stage->addItem("Rainbow Wave Runner");
            ui->cmbBox_Stage->addItem("Rainbow Wave");
            ui->cmbBox_Stage->addItem("UpDown Colour");
            ui->cmbBox_Stage->addItem("SlowRun Colour");
        }
        break;
        case 3:
        {
            isRgb=false;
            isHsv=false;
            isCmyk=true;
        }
        break;
        default:
        {
            isRgb=false;
            isHsv=false;
            isCmyk=false;
        }
        break;
    }
}

void MainWindow::on_cmbBox_Stage_currentIndexChanged(int index)
{
    currStadia=index;
    if ((isHsv && index==1) || (isRgb && (index==2 || index==3)))
    {
        ui->spinBox->setEnabled(false);
        if (isRgb)
            {ui->spinBox->setValue(4);}
        if (isHsv)
        {
            ui->spinBox->setMinimum(0);
            if (index==1)
                {ui->spinBox->setValue(0);}
            else
                {ui->spinBox->setValue(int(NUM_LEDS));}
        }
    }
    else
    {
        ui->spinBox->setEnabled(true);
        if (isRgb)
        {
            if (index==1)
            {
                ui->spinBox->setMinimum(2);
            }
        }
        if (isHsv)
        {
            if (index==2)
            {
                ui->spinBox->setMinimum(0);
                ui->spinBox->setMaximum(int(NUM_LEDS));
                ui->spinBox->setValue(1);
            }
            if (index==3)
            {
                ui->spinBox->setMinimum(1);
                ui->spinBox->setMaximum(360);
                ui->spinBox->setValue(1);
            }
        }
    }
}

void MainWindow::vTaskDelay(int delay)
{
    if (Timer->interval()!=delay)
    {
        Timer->stop();
        Timer->setInterval(delay);
        Timer->start();
    }
}

int MainWindow::getValue(int min, int max)
{
    QRandomGenerator generator;
    return(generator.bounded(min,max));
}

void MainWindow::stadia_rgb_1()
{
    RGB_t rgbStage[(u_int8_t) ui->spinBox->value()];
    ac->setLedsStage1RGB(rgbStage, (u_int8_t) ui->spinBox->value());
    app->MoveLights(leds_rgb, (uint8_t) NUM_LEDS, rgbStage, (u_int8_t) ui->spinBox->value(), &DeferenceIndex, (uint8_t) QVariant(ui->cbReverse->isChecked()).toInt());
    LG->setRGBState(leds_rgb);
    vTaskDelay(TQR*ui->doubleSpinBox->value());
}

void MainWindow::stadia_rgb_2()
{
    RGB_t rgbStage[4];
    ac->setLedsStageRGB_RGB(rgbStage);
    app->MoveLights(leds_rgb, (uint8_t) NUM_LEDS, rgbStage, 4, &DeferenceIndex, (uint8_t) QVariant(ui->cbReverse->isChecked()).toInt());
    LG->setRGBState(leds_rgb);
    vTaskDelay(TQR*2);
}
void MainWindow::stadia_rgb_3()
{
    RGB_t rgbStage[4];
    ac->setLedsStageCMYK_RGB(rgbStage);
    app->MoveLights(leds_rgb, (uint8_t) NUM_LEDS, rgbStage, 4, &DeferenceIndex, (uint8_t) QVariant(ui->cbReverse->isChecked()).toInt());
    LG->setRGBState(leds_rgb);
    vTaskDelay(TQR);
}

void MainWindow::stadia_hsv_1()
{
    HSV_t hsvStage[1];
    hsvStage[0]=ac->getColor.setRed_hsv();
    app->MoveColor(leds_hsv, (uint8_t) NUM_LEDS, hsvStage, 1, &DeferenceIndex, (uint8_t) QVariant(ui->cbReverse->isChecked()).toInt());
    LG->setHSVState(leds_hsv);
    vTaskDelay(TQR/6);
}

void MainWindow::stadia_hsv_2()
{
    uint16_t changeLedHue;
    uint16_t ledsLimit;
    HSV_t hsvStage[1];
    hsvStage[0]=ac->getColor.setRed_hsv();
    if (360%int(NUM_LEDS)==0)
        {changeLedHue=(uint16_t) 360/NUM_LEDS;}
    else
        {changeLedHue=(uint16_t) 360/NUM_LEDS+1;}
    if (ui->spinBox->value()==0 || ((uint16_t) ui->spinBox->value())*changeLedHue==360)
        {ledsLimit=uint16_t(ui->spinBox->value());}
    else
    {
        changeLedHue=uint16_t(360 / ui->spinBox->value());
        ledsLimit=360/changeLedHue;
    }
    app->RainbowWave(leds_hsv, (uint8_t) NUM_LEDS, hsvStage, 1, &DeferenceIndex, changeLedHue, ledsLimit, &fTimer);
    LG->setHSVState(leds_hsv);
    vTaskDelay(TQR/30);
}

void MainWindow::stadia_hsv_3()
{
    HSV_t hsvStage[(uint16_t) ui->spinBox->value()];
    ac->setHSVWave(hsvStage, uint16_t(ui->spinBox->value()));
    app->MoveColor(leds_hsv, (uint8_t) NUM_LEDS, hsvStage, (uint16_t) ui->spinBox->value(), &DeferenceIndex, (uint8_t) QVariant(ui->cbReverse->isChecked()).toInt());
    LG->setHSVState(leds_hsv);
    vTaskDelay(TQR/60);
}

void MainWindow::stadia_hsv_4()
{
    uint16_t changeLedHue;
    // uint16_t ledsLimit;
    if (ui->spinBox->value()==0)
        {changeLedHue=360;}
    else
        {changeLedHue=ui->spinBox->value();}
    while (360%changeLedHue!=0)
        {changeLedHue++;}
    HSV_t hsvStage[changeLedHue];
    for (int i=0;i<changeLedHue;i++)
    {
        if (i==0)
            {hsvStage[i].h=0;}
        else
            {hsvStage[i].h=360/changeLedHue*i;}
        hsvStage[i].s=255;
        hsvStage[i].v=0;
    }
    app->MoveHSV(leds_hsv, (uint8_t) NUM_LEDS, hsvStage, changeLedHue, &DeferenceIndex);
    LG->setHSVState(leds_hsv);
    vTaskDelay(TQR/60);
}

void MainWindow::stadia_hsv_5()
{
    uint16_t changeLedHue;
    uint16_t ledsLimit;
    if (ui->spinBox->value()==0)
        {changeLedHue=360;}
    else
        {changeLedHue=ui->spinBox->value();}
    while (360%changeLedHue!=0)
        {changeLedHue++;}
    HSV_t hsvStage[changeLedHue];
    for (int i=0;i<changeLedHue;i++)
    {
        if (i==0)
            {hsvStage[i].h=0;}
        else
            {hsvStage[i].h=360/changeLedHue*i;}
        hsvStage[i].s=255;
        hsvStage[i].v=0;
    }
    //app->UpDownMove(leds_hsv, (uint8_t) NUM_LEDS, hsvStage, changeLedHue, &DeferenceIndex);
    LG->setHSVState(leds_hsv);
    vTaskDelay(TQR/60);
}
