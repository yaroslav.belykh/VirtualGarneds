#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QResizeEvent>
#include <QRandomGenerator>
#include <QTimer>
#include <QSize>

#include "ledsgarlend.h"
#include "application.h"
#include "makecollrsarray.h"
#include "structure_led.h"
#include "structure_widgets.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

signals:
    void sizeChanged(QSize, bool);

protected:
    void resizeEvent(QResizeEvent *event);

private slots:
    void doActions(void);
    void doneActions(void);
    void timeoutDebug(void);

    void stadia_rgb_1(void);
    void stadia_rgb_2(void);
    void stadia_rgb_3(void);
    void stadia_hsv_1(void);
    void stadia_hsv_2(void);
    void stadia_hsv_3(void);
    void stadia_hsv_4(void);
    void stadia_hsv_5(void);

    void restoreGarlend(QSize, bool);

    void on_pushButton_toggled(bool checked);

    void on_cmbBox_Color_currentIndexChanged(int index);

    void on_cmbBox_Stage_currentIndexChanged(int index);

private:
    Ui::MainWindow *ui;

    LedsGarlend *LG;
    Application *app;
    MakeColorsArray *ac;

    QTimer *Timer;

    //volatile uint8_t StageColors[];

    //volatile
    RGB_t leds_rgb[NUM_LEDS];
    //volatile
    HSV_t leds_hsv[NUM_LEDS];

    //volatile
    uint16_t DeferenceIndex;
    uint8_t fTimer=0;

    int currStadia;

    bool isRgb;
    bool isHsv;
    bool isCmyk;

    tLEDs LEDs;

    void vTaskDelay(int);

    inline int getValue(int min = 0, int max = 255 );
};
#endif // MAINWINDOW_H
