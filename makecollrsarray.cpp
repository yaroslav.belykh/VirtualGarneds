#include "makecollrsarray.h"

MakeColorsArray::MakeColorsArray(QObject *parent) : QObject(parent)
{

}

void MakeColorsArray::setHSVWave(HSV_t *hsv, uint16_t value)
{
    uint16_t engelHSV;
    if (value!=0)
        {engelHSV=360/value;}
    else
        {engelHSV=0;}
    for (uint16_t i=0;i<value;i++)
    {
        hsv[i].h=i*engelHSV;
        hsv[i].s=255;
        hsv[i].v=255;
    }
}

void MakeColorsArray::setOneColorLeds_rgb(RGB_t *LEDs, uint8_t countLEDs, uint8_t rgbRed, uint8_t rgbGreen, uint8_t rgbBlue, uint8_t *isRGB)
{
   // taskENTER_CRITICAL();
    for (int i=0;i<countLEDs;i++)
    {
        LEDs[i].r=rgbRed;
        LEDs[i].g=rgbGreen;
        LEDs[i].b=rgbBlue;
    }
    //taskEXIT_CRITICAL();
    //*isRGB=1;
}

void MakeColorsArray::setOneColorLeds_hsv(HSV_t *LEDs, uint8_t countLEDs, uint16_t hsvHue, uint8_t hsvSaturation, uint8_t hsvValue, uint8_t *isRGB)
{
    //taskENTER_CRITICAL();
    for (int i=0;i<countLEDs;i++)
    {
        LEDs[i].h=hsvHue;
        LEDs[i].s=hsvSaturation;
        LEDs[i].v=hsvValue;
    }
    //taskEXIT_CRITICAL();
    //*isRGB=;
}

void MakeColorsArray::setLedsStageRGB_RGB(RGB_t *rgb)
{
    rgb[0]=getColor.setWhight_rgb();
    rgb[1]=getColor.setRed_rgb();
    rgb[2]=getColor.setGreen_rgb();
    rgb[3]=getColor.setBlue_rgb();
    //rgb[4]=getColor.setBlack_rgb();
}

void MakeColorsArray::setLedsStageCMYK_RGB(RGB_t *rgb)
{
    rgb[0]=getColor.setCyan_rgb();
    rgb[1]=getColor.setMagenta_rgb();
    rgb[2]=getColor.setYellow_rgb();
    rgb[3]=getColor.setBlack_rgb();
}

void MakeColorsArray::setLedsStage1RGB(RGB_t *rgb, uint8_t count)
{
    if (count>0)
    {
        rgb[0]=getColor.setRed_rgb();
        if (count>1)
        {
            rgb[1]=getColor.setGreen_rgb();
            if (count>2)
            {
                rgb[2]=getColor.setBlue_rgb();
                if (count>3)
                {
                    rgb[3]=getColor.setYellow_rgb();
                    if (count>4)
                    {
                        rgb[4]=getColor.setOrange_rgb();
                        if (count>5)
                        {
                            rgb[5]=getColor.setViolet_rgb();
                            if (count>6)
                            {
                                rgb[6]=getColor.setEmerald_rgb();
                                if (count>7)
                                {
                                    rgb[7]=getColor.setRose_rgb();
                                    if (count>8)
                                    {
                                        rgb[8]=getColor.setAmber_rgb();
                                        if (count>9)
                                        {
                                            rgb[9]=getColor.setUltramarine_rgb();
                                            if (count>10)
                                            {
                                                rgb[10]=getColor.setBlack_rgb();
                                                if (count>11)
                                                {
                                                    rgb[11]=getColor.setWhight_rgb();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

void MakeColorsArray::setLedsStage1HSV(HSV_t *hsv, uint8_t count)
{
    if (count>0)
    {
        hsv[0]=getColor.setRed_hsv();
        if (count>1)
        {
            hsv[1]=getColor.setGreen_hsv();
            if (count>2)
            {
                hsv[2]=getColor.setBlue_hsv();
                if (count>3)
                {
                    hsv[3]=getColor.setYellow_hsv();
                    if (count>4)
                    {
                        hsv[4]=getColor.setOrange_hsv();
                        if (count>5)
                        {
                            hsv[5]=getColor.setViolet_hsv();
                            if (count>6)
                            {
                                hsv[6]=getColor.setMagenta_hsv();
                                if (count>7)
                                {
                                    hsv[7]=getColor.setCyan_hsv();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
