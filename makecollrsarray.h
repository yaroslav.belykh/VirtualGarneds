#ifndef MAKECOLLRSARRAY_H
#define MAKECOLLRSARRAY_H

#include <QObject>

#include "colors.h"

class MakeColorsArray : public QObject
{
    Q_OBJECT
public:
    explicit MakeColorsArray(QObject *parent = nullptr);

    Colors getColor;

    void setHSVWave(HSV_t *hsv, uint16_t value);

    void setOneColorLeds_rgb(RGB_t *LEDs, uint8_t countLEDs, uint8_t rgbRed, uint8_t rgbGreen, uint8_t rgbBlue, uint8_t *isRGB);
    void setOneColorLeds_hsv(HSV_t *LEDs, uint8_t countLEDs, uint16_t hsvHue, uint8_t hsvSaturation, uint8_t hsvValue, uint8_t *isRGB);

    void setLedsStageRGB_RGB(RGB_t *rgb);
    void setLedsStageCMYK_RGB(RGB_t *rgb);
    void setLedsStage1RGB(RGB_t *rgb, uint8_t count);
    void setLedsStage1HSV(HSV_t *rgb, uint8_t count);
    void setLedsStage3RGB(RGB_t *rgb);
    void setLedsStage4RGB(RGB_t *rgb);

    void setLedsStage1HSV(HSV_t *hsv);
    void setLedsStage2HSV(HSV_t *hsv);
    void setLedsStage2HSV_Off(HSV_t *hsv);
    void setLedsStage4HSV(HSV_t *hsv);
    void setLedsStage4HSV_Off(HSV_t *hsv);

signals:

public slots:

private:

};

#endif // MAKECOLLRSARRAY_H
