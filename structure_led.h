#ifndef STRUCTURES_H
#define STRUCTURES_H

#include <stdint.h>
#define TQR  1000
#define NUM_LEDS    254

struct RGB
{
    uint8_t r;
    uint8_t g;
    uint8_t b;
};

struct HSV
{
    uint16_t h;
    uint8_t s;
    uint8_t v;
};

struct CMYK
{
    uint8_t c;
    uint8_t m;
    uint8_t y;
    uint8_t k;
};

typedef struct RGB RGB_t;
typedef struct HSV HSV_t;
typedef struct CMYK CMYK_t;

#define HUE(h)  ({ typeof(h) h1 = h % 360; h1 < 0 ? 360 + h1 : h1; })



#endif // STRUCTURES_H
