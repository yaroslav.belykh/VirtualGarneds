#include "virtualled.h"
#include "ui_virtualled.h"

VirtualLed::VirtualLed(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::VirtualLed)
{
    ui->setupUi(this);
    Color.setRgb(0,0,0,255);
}

VirtualLed::~VirtualLed()
{
    delete ui;
}

void VirtualLed::setSize(int x, int y)
{
    this->setMaximumWidth(x);
    this->setMaximumHeight(y);
    this->setMinimumWidth(x);
    this->setMinimumHeight(y);
}

void VirtualLed::setColor(QColor clr)
{
    Color=clr;
    repaint();
}

void VirtualLed::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter painter(this);
    painter.setPen(QPen(Qt::black, 1, Qt::SolidLine, Qt::FlatCap));
    painter.setBrush(QBrush(Color, Qt::SolidPattern));
    painter.drawRect(getRect());
}

QRect VirtualLed::getRect()
{
    int x;
    int y;
    int w;
    int h;
    x=1;
    y=1;
    w=this->maximumWidth()-2*x;
    h=this->maximumHeight()-2*y;
    QRect rect(x,y,w,h);
    return(rect);
}

