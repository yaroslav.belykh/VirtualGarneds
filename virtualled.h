#ifndef VIRTUALLED_H
#define VIRTUALLED_H

#include <QFrame>
#include <QPainter>
#include <QColor>

namespace Ui {
class VirtualLed;
}

class VirtualLed : public QFrame
{
    Q_OBJECT

public:
    explicit VirtualLed(QWidget *parent = nullptr);
    ~VirtualLed();

    void setSize(int,int);
    void setColor(QColor);

protected:
    void paintEvent(QPaintEvent *event);

private:
    Ui::VirtualLed *ui;

    QColor Color;
    inline QRect getRect(void);
};

#endif // VIRTUALLED_H
